package ar.fiuba.tdd.tp1.integrationtest;

import ar.fiuba.tdd.tp1.api.ConcreteWorkbookControllerBuilder;
import ar.fiuba.tdd.tp1.api.DuplicateBookNameException;
import ar.fiuba.tdd.tp1.api.DuplicateSheetNameException;
import ar.fiuba.tdd.tp1.api.ISheetCell;
import ar.fiuba.tdd.tp1.api.ISpreadsheet;
import ar.fiuba.tdd.tp1.api.IWorkbook;
import ar.fiuba.tdd.tp1.api.IWorkbookController;
import ar.fiuba.tdd.tp1.api.InvalidBookNameException;
import ar.fiuba.tdd.tp1.api.InvalidCellIDException;
import ar.fiuba.tdd.tp1.api.InvalidSheetNameException;
import ar.fiuba.tdd.tp1.workbook.DetectCycle;

import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ISpreadsheetTest {

    private ISpreadsheet createSheet() {

        ConcreteWorkbookControllerBuilder builder = new ConcreteWorkbookControllerBuilder();
        IWorkbookController controller = builder.buildWorkbookController();
        try {
            controller.addBook("book1");
            IWorkbook book = controller.getBook("book1");
            book.addSheet("sheet1");
            return book.getSheet("sheet1", true);
        } catch (DuplicateBookNameException e) {
            e.printStackTrace();
        } catch (InvalidBookNameException e) {
            e.printStackTrace();
        } catch (DuplicateSheetNameException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvalidSheetNameException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    @Test
    public void getName() {
        ISpreadsheet sheet = createSheet();
        assertTrue(sheet.getName() == "sheet1");
    }

    @Test
    public void getNewCell() {
        ISpreadsheet sheet = createSheet();
        try {
            sheet.getCell("A1", true);
        } catch (InvalidCellIDException e) {
            fail();
        }
    }
    
    @Test
    public void cellsWithCycle() {
        ISpreadsheet sheet = createSheet();
        DetectCycle cycleDetector = new DetectCycle();
        try {
            ISheetCell cellA1 = sheet.getCell("A1", true);
            ISheetCell cellA2 = sheet.getCell("A2", true);
            ISheetCell cellA3 = sheet.getCell("A3", true);
            ISheetCell cellA4 = sheet.getCell("A4", true);
            try {
                cellA1.setValue("= 1 + A2");
                cellA2.setValue("= A3");
                cellA3.setValue("= A1");
                cellA4.setValue("=8");
                cycleDetector.addCells(sheet.getCells());
                assertTrue(cycleDetector.hasCycle(cellA1.getId()) == true);
                assertTrue(cycleDetector.hasCycle(cellA4.getId()) == false);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (InvalidCellIDException e) {
            fail();
        }
    }
    
    @Test
    public void cellsMultipleCycleCheck() {
        ISpreadsheet sheet = createSheet();
        DetectCycle cycleDetector = new DetectCycle();
        try {
            ISheetCell cellA1 = sheet.getCell("A1", true);
            ISheetCell cellA2 = sheet.getCell("A2", true);
            ISheetCell cellA3 = sheet.getCell("A3", true);
            try {
                cellA1.setValue("A2");
                cellA2.setValue("A3");
                cellA3.setValue("A4");
                cycleDetector.addCells(sheet.getCells());
                assertTrue(cycleDetector.hasCycle(cellA1.getId()) == false);
                ISheetCell cellA4 = sheet.getCell("A4", true);
                cellA4.setValue("A2");
                cycleDetector.addCells(sheet.getCells());
                assertTrue(cycleDetector.hasCycle(cellA1.getId()) == false);
                assertTrue(cycleDetector.hasCycle(cellA2.getId()) == true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (InvalidCellIDException e) {
            fail();
        }
    }
    
    @Test
    public void cellsWithoutCycle() {
        ISpreadsheet sheet = createSheet();
        DetectCycle cycleDetector = new DetectCycle();
        try {
            ISheetCell cellA1 = sheet.getCell("A1", true);
            ISheetCell cellA2 = sheet.getCell("A2", true);
            ISheetCell cellA3 = sheet.getCell("A3", true);
            try {
                cellA1.setValue("A2");
                cellA2.setValue("A3");
                cellA3.setValue("=2+1");
                cycleDetector.addCells(sheet.getCells());
                assertTrue(cycleDetector.hasCycle(cellA1.getId()) == false);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (InvalidCellIDException e) {
            fail();
        }
    }
    
    @Test
    public void getSameCellMultipleTimes() {
        ISpreadsheet sheet = createSheet();
        try {
            sheet.getCell("A1", true);
            sheet.getCell("A1", true);
            sheet.getCell("A1", true);
        } catch (InvalidCellIDException e) {
            fail();
        }
    }

    @Test(expected = InvalidCellIDException.class)
    public void getInvalidCell() throws InvalidCellIDException {
        ISpreadsheet sheet = createSheet();
        sheet.getCell("A---1", true);
    }

}
