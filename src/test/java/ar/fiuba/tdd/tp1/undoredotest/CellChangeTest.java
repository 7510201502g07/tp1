package ar.fiuba.tdd.tp1.undoredotest;

import ar.fiuba.tdd.tp1.api.ISheetCell;
import ar.fiuba.tdd.tp1.undoredo.CellChange;
import ar.fiuba.tdd.tp1.workbook.ConcreteCell;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class CellChangeTest {

    private ISheetCell cell   = new ConcreteCell("A1");
    private ISheetCell cell2  = new ConcreteCell("A2");
    private ISheetCell cell3  = new ConcreteCell("A3");
    private String     value  = "=20";
    private String     value2 = "=30";
    private String     value3 = "=40";
    private String     value4 = "=50";
    private String     value5 = "=60";
    private String     value6 = "=1234";

    @Test
    public void singleRevertCell() throws Exception {
        cell.setValue(value);
        assertTrue(cell.getRawValue() == "=20");
        CellChange cellChange = new CellChange(cell, cell.getRawValue(), value6);
        cell.setValue(value2);
        assertTrue(cell.getRawValue() == "=30");
        cellChange.revert();
        assertTrue(cell.getRawValue() == "=20");
    }

    @Test
    public void doubleRevertCell() {
        CellChange cellChange = new CellChange(cell, value, value6);
        CellChange cellChange2 = new CellChange(cell, value2, value6);
        cellChange2.revert();
        cellChange.revert();
        assertTrue(cell.getRawValue() == "=20");
    }

    @Test
    public void singleRevertWithTwoCells() throws Exception {
        cell.setValue(value);
        cell2.setValue(value2);
        CellChange cell2Change = new CellChange(cell2, cell2.getRawValue(), value6);
        cell2.setValue(value3);
        assertTrue(cell2.getRawValue() == "=40");
        cell2Change.revert();
        assertTrue(cell2.getRawValue() == "=30");
        assertTrue(cell.getRawValue() == "=20");
    }

    @Test
    public void doubleRevertWithTwoCells() {
        CellChange cell2Change = new CellChange(cell2, value2, value6);
        CellChange cellChange = new CellChange(cell, value, value6);
        cell2Change.revert();
        cellChange.revert();
        assertTrue(cell2.getRawValue() == "=30");
        assertTrue(cell.getRawValue() == "=20");
    }

    @Test
    public void multipleRevertWithMultipleCells() {
        CellChange cellChange = new CellChange(cell, value, value6);
        CellChange cellChange2 = new CellChange(cell, value2, value6);
        cellChange2.revert();
        cellChange.revert();
        CellChange cell2Change = new CellChange(cell2, value2, value6);
        CellChange cell2Change2 = new CellChange(cell2, value3, value6);
        cell2Change2.revert();
        cell2Change.revert();
        CellChange cell3Change3 = new CellChange(cell3, value5, value6);
        CellChange cell3Change2 = new CellChange(cell3, value4, value6);
        CellChange cell3Change = new CellChange(cell3, value3, value6);
        cell3Change3.revert();
        cell3Change2.revert();
        cell3Change.revert();
        assertEquals(cell.getRawValue(), "=20");
        assertEquals(cell2.getRawValue(), "=30");
        assertEquals(cell3.getRawValue(), "=40");
    }

    @Test
    public void singleRevertWrongCell() throws Exception {
        cell.setValue(value);
        cell2.setValue(value2);
        CellChange cellChange = new CellChange(cell2, cell2.getRawValue(), value6);
        cell.setValue(value2);
        cellChange.revert();
        assertNotEquals(cell.getRawValue(), "=20");
    }

    @Test
    public void doubleRevertWrongOrder() {
        CellChange cellChange = new CellChange(cell, value, value6);
        CellChange cellChange2 = new CellChange(cell, value2, value6);
        cellChange.revert();
        cellChange2.revert();
        assertNotEquals(cell.getRawValue(), "=20");
        assertEquals(cell.getRawValue(), "=30");
    }

    @Test
    public void multipleRevertCellWithOneValue() throws Exception {
        cell.setValue(value);
        CellChange cellChange = new CellChange(cell, cell.getRawValue(), value6);
        cell.setValue(value2);
        cellChange.revert();
        assertTrue(cell.getRawValue() == "=20");
        cellChange.revert();
        cellChange.revert();
        cellChange.revert();
        cellChange.revert();
        cellChange.revert();
        assertTrue(cell.getRawValue() == "=20");
    }
}