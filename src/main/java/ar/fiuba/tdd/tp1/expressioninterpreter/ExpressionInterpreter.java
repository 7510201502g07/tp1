package ar.fiuba.tdd.tp1.expressioninterpreter;

import ar.fiuba.tdd.tp1.api.InvalidBookNameException;
import ar.fiuba.tdd.tp1.api.InvalidFormulaException;
import ar.fiuba.tdd.tp1.api.InvalidSheetNameException;
import ar.fiuba.tdd.tp1.contextfreegrammar.ExtendedContextFreeGrammar;
import ar.fiuba.tdd.tp1.parser.LR1Parser;
import ar.fiuba.tdd.tp1.parser.ParseTree;

import java.util.List;

// Acts as a facade that separates the parsing and interpreting of cell grammars
// from the rest of the application, instead providing a much simpler interface 
// where the application doesn't have to worry about cell grammar and building
// the parser and interpreter.
public class ExpressionInterpreter {

    static ExpressionInterpreter     parserInstance = buildParser();

    private LR1Parser                parser;
    private CellParseTreeInterpreter interpreter;

    private ExpressionInterpreter() {
    }

    public static ExpressionInterpreter getInstance() {
        return parserInstance;
    }

    public ParseTree parse(String toParse) throws Exception {
        return this.parser.parse(toParse);
    }

    public String interpret(ParseTree parseTree)
            throws InvalidSheetNameException, InvalidBookNameException, InvalidFormulaException {
        return interpreter.interpret(parseTree);
    }

    private static ExpressionInterpreter buildParser() {

        ExtendedContextFreeGrammar grammar = buildGrammar();
        LR1Parser parser = null;
        try {
            parser = new LR1Parser(grammar);
        } catch (Exception e) {
            // This code should never execute.
            e.printStackTrace();
        }

        ExpressionInterpreter expressionParser = new ExpressionInterpreter();
        expressionParser.parser = parser;
        expressionParser.interpreter = new CellParseTreeInterpreter(grammar);
        return expressionParser;
    }

    private static ExtendedContextFreeGrammar buildGrammar() {

        ExtendedContextFreeGrammar grammar = new ExtendedContextFreeGrammar();

        try {
            loadTerminals(grammar);
            loadNonterminals(grammar);
            loadProductionRules(grammar);
            grammar.registerStartSymbol(CellGrammar.startSymbol);
            grammar.lock();
        } catch (Exception e) {
            // This code should never execute.
            e.printStackTrace();
        }

        return grammar;
    }

    private static void loadTerminals(ExtendedContextFreeGrammar grammar) throws Exception {

        List<String> terminals = CellGrammar.getTerminals();
        List<String> regex = CellGrammar.getRegex();

        for (int i = 0; i < terminals.size(); i++) {
            grammar.registerTerminalSymbol(terminals.get(i), regex.get(i));
        }
    }

    private static void loadNonterminals(ExtendedContextFreeGrammar grammar) throws Exception {

        List<String> nonterminals = CellGrammar.getNonterminals();
        List<CellParseTreeInterpreterAction> actions = CellGrammar.getActions();

        for (int i = 0; i < nonterminals.size(); i++) {
            grammar.registerNonterminalSymbol(nonterminals.get(i), actions.get(i));
        }
    }

    private static void loadProductionRules(ExtendedContextFreeGrammar grammar) throws Exception {

        for (List<String> productionRule : CellGrammar.getProductionRules()) {
            grammar.registerProductionRule(productionRule.get(0), productionRule.get(1), productionRule.get(2));
        }
    }
}
