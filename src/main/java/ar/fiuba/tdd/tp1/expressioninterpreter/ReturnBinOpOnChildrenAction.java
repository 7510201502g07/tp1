package ar.fiuba.tdd.tp1.expressioninterpreter;

import ar.fiuba.tdd.tp1.api.InvalidBookNameException;
import ar.fiuba.tdd.tp1.api.InvalidFormulaException;
import ar.fiuba.tdd.tp1.api.InvalidSheetNameException;
import ar.fiuba.tdd.tp1.parser.ParseTree;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;

public class ReturnBinOpOnChildrenAction implements CellParseTreeInterpreterAction {

    private Map<String, BinaryOperator<Float>> binaryOps;

    ReturnBinOpOnChildrenAction() {
        binaryOps = new HashMap<>();
        binaryOps.put(CellGrammar.PLUS, (opA, opB) -> opA + opB);
        binaryOps.put(CellGrammar.MINUS, (opA, opB) -> opA - opB);
        binaryOps.put(CellGrammar.TIMES, (opA, opB) -> opA * opB);
        binaryOps.put(CellGrammar.DIVIDE, (opA, opB) -> opA / opB);
    }

    @Override
    public String apply(ParseTree tree, Map<String, CellParseTreeInterpreterAction> actionMap)
            throws InvalidSheetNameException, InvalidBookNameException, InvalidFormulaException, NumberFormatException {

        List<ParseTree> children = tree.getChildren();

        // A sum or term can consist of only 1 factor.
        if (children.size() == 1) {
            return actionMap.get(children.get(0).getNode().getType()).apply(children.get(0), actionMap);
        }

        float firstOperand = Float
                .valueOf(actionMap.get(children.get(0).getNode().getType()).apply(children.get(0), actionMap));
        float secondOperand = Float
                .valueOf(actionMap.get(children.get(2).getNode().getType()).apply(children.get(2), actionMap));
        float result = binaryOps.get(children.get(1).getNode().getType()).apply(firstOperand, secondOperand);
        return Float.toString(result);
    }

}
