package ar.fiuba.tdd.tp1.expressioninterpreter;

import ar.fiuba.tdd.tp1.api.InvalidBookNameException;
import ar.fiuba.tdd.tp1.api.InvalidFormulaException;
import ar.fiuba.tdd.tp1.api.InvalidSheetNameException;
import ar.fiuba.tdd.tp1.parser.ParseTree;

import java.util.Map;

public class ReturnChildValueAction implements CellParseTreeInterpreterAction {

    int childPosition;

    public ReturnChildValueAction(int position) {
        this.childPosition = position;
    }

    @Override
    public String apply(ParseTree tree, Map<String, CellParseTreeInterpreterAction> actionMap)
            throws NumberFormatException, InvalidSheetNameException, InvalidBookNameException, InvalidFormulaException {

        ParseTree child = tree.getChildren().get(childPosition);
        String childType = child.getNode().getType();
        return actionMap.get(childType).apply(child, actionMap);
    }

}
