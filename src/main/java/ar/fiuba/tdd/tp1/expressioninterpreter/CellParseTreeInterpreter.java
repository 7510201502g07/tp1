package ar.fiuba.tdd.tp1.expressioninterpreter;

import ar.fiuba.tdd.tp1.api.InvalidBookNameException;
import ar.fiuba.tdd.tp1.api.InvalidFormulaException;
import ar.fiuba.tdd.tp1.api.InvalidSheetNameException;
import ar.fiuba.tdd.tp1.contextfreegrammar.ExtendedContextFreeGrammar;
import ar.fiuba.tdd.tp1.parser.ParseTree;

import java.util.HashMap;
import java.util.Map;

// Uses the rules defined by an extendedContextFreeGrammar to interpret
// the output parseTree of a parser.
public class CellParseTreeInterpreter {

    ExtendedContextFreeGrammar                  grammar;
    Map<String, CellParseTreeInterpreterAction> actionMap;

    public CellParseTreeInterpreter(ExtendedContextFreeGrammar grammar) {

        this.grammar = grammar;
        this.actionMap = new HashMap<String, CellParseTreeInterpreterAction>();
        loadActionMap();
    }

    private void loadActionMap() {

        for (String nonterminal : grammar.getNonterminalSymbols()) {
            CellParseTreeInterpreterAction action = grammar.getActionForNonterminal(nonterminal);
            this.actionMap.put(nonterminal, action);
        }
    }

    public String interpret(ParseTree tree)
            throws InvalidSheetNameException, InvalidBookNameException, InvalidFormulaException {
        return this.actionMap.get(grammar.getStartSymbol()).apply(tree, this.actionMap);
    }
}
