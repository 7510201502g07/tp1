package ar.fiuba.tdd.tp1.expressioninterpreter;

import ar.fiuba.tdd.tp1.api.InvalidBookNameException;
import ar.fiuba.tdd.tp1.api.InvalidFormulaException;
import ar.fiuba.tdd.tp1.api.InvalidSheetNameException;
import ar.fiuba.tdd.tp1.parser.ParseTree;

import java.util.Map;

public class ReturnConcatAction implements CellParseTreeInterpreterAction {

    @Override
    public String apply(ParseTree tree, Map<String, CellParseTreeInterpreterAction> actionMap)
            throws InvalidSheetNameException, InvalidBookNameException, InvalidFormulaException, NumberFormatException {

        ParseTree bottomTree = tree.getChildren().get(2);
        while (bottomTree.getChildren().get(0).getNode().getType() == CellGrammar.VALUES_LIST) {
            bottomTree = bottomTree.getChildren().get(0);
        }

        String auxValue = getValue(bottomTree, actionMap, 0);
        StringBuffer buf = new StringBuffer();
        buf.append(auxValue);
        bottomTree = bottomTree.getParent();

        while (bottomTree.getNode().getType() != CellGrammar.CONCAT_VALUES) {
            auxValue = getValue(bottomTree, actionMap, 2);
            buf.append(auxValue);
            bottomTree = bottomTree.getParent();
        }
        return buf.toString();
    }

    private String getValue(ParseTree tree, Map<String, CellParseTreeInterpreterAction> actionMap, int pos)
            throws NumberFormatException, InvalidSheetNameException, InvalidBookNameException, InvalidFormulaException {
        if (tree.getChildren().get(pos).getNode().getType() == CellGrammar.STRING_LITERAL) {
            // TODO: remove quotes.
            return tree.getChildren().get(pos).getNode().getValue();
        } else {
            return actionMap.get(CellGrammar.MATH_EXPRESSION).apply(tree.getChildren().get(pos), actionMap);
        }
    }

}
