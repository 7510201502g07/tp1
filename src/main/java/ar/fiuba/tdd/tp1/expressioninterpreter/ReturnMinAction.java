package ar.fiuba.tdd.tp1.expressioninterpreter;

public class ReturnMinAction extends ReturnComparisonOnRangeAction {

    @Override
    protected boolean betterMatch(float value, float currentResultValue) {
        return value < currentResultValue;
    }

}
