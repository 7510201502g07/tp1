package ar.fiuba.tdd.tp1.expressioninterpreter;

import ar.fiuba.tdd.tp1.api.InvalidBookNameException;
import ar.fiuba.tdd.tp1.api.InvalidFormulaException;
import ar.fiuba.tdd.tp1.api.InvalidSheetNameException;
import ar.fiuba.tdd.tp1.parser.ParseTree;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.UnaryOperator;

public class ReturnUnaryOpOnChildrenAction implements CellParseTreeInterpreterAction {

    private Map<String, UnaryOperator<Float>> unaryOps;

    ReturnUnaryOpOnChildrenAction() {
        unaryOps = new HashMap<>();
        unaryOps.put(CellGrammar.PLUS, (opA) -> opA);
        unaryOps.put(CellGrammar.MINUS, (opA) -> -opA);
    }

    @Override
    public String apply(ParseTree tree, Map<String, CellParseTreeInterpreterAction> actionMap)
            throws InvalidSheetNameException, InvalidBookNameException, InvalidFormulaException, NumberFormatException {
        List<ParseTree> children = tree.getChildren();
        return Float.toString(unaryOps.get(children.get(0).getNode().getType()).apply(
                Float.valueOf(actionMap.get(children.get(1).getNode().getType()).apply(children.get(1), actionMap))));
    }

}
