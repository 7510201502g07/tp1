package ar.fiuba.tdd.tp1.expressioninterpreter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

// Describes the grammar accepted by cells in a spreadsheet.
public class CellGrammar {

    private CellGrammar() {
    }

    @SuppressWarnings("CPD-START")
    public static final String                                EQUAL           = "equal";
    public static final String                                NUM_VALUE       = "numValue";
    public static final String                                REF_VALUE       = "refValue";
    public static final String                                PLUS            = "plus";
    public static final String                                MINUS           = "minus";
    public static final String                                TIMES           = "times";
    public static final String                                DIVIDE          = "divide";
    public static final String                                OPEN_PAR        = "openParentheses";
    public static final String                                CLOSE_PAR       = "closeParentheses";
    public static final String                                STR_VALUE       = "strValue";
    public static final String                                MAX             = "max";
    public static final String                                MIN             = "min";
    public static final String                                AVERAGE         = "average";
    public static final String                                RANGE           = "range";
    public static final String                                CONCAT          = "concat";
    public static final String                                COMMA           = "comma";
    public static final String                                STRING_LITERAL  = "stringLiteral";

    public static final String                                CELL_VALUE      = "cellValue";
    public static final String                                STRING          = "string";
    public static final String                                FORMULA         = "formula";
    public static final String                                MATH_EXPRESSION = "mathExpression";
    public static final String                                SUM             = "sum";
    public static final String                                TERM            = "term";
    public static final String                                FACTOR          = "factor";
    public static final String                                UNARY_SUM       = "unarySum";
    public static final String                                MAX_RANGE       = "maxRange";
    public static final String                                MIN_RANGE       = "minRange";
    public static final String                                AVERAGE_RANGE   = "averageRange";
    public static final String                                VALUES_LIST     = "valuesList";
    public static final String                                CONCAT_VALUES   = "concatValues";

    private static final List<String>                         terminals       = Collections
            .unmodifiableList(Arrays.asList(EQUAL, NUM_VALUE, REF_VALUE, PLUS, MINUS, TIMES, DIVIDE, OPEN_PAR,
                    CLOSE_PAR, STR_VALUE, MAX, MIN, AVERAGE, RANGE, CONCAT, COMMA, STRING_LITERAL));

    private static final List<String>                         regex           = Collections.unmodifiableList(Arrays
            .asList("^= *", " *[0-9]*\\.[0-9]+|[0-9]+ *", " *((\\[[a-zA-Z_0-9]+\\])?[a-zA-Z_0-9]+!)?[A-Z]+[0-9]+ *",
                    " *\\+ *", " *\\- *", " *\\* *", " *\\/ *", " *\\( *", " *\\) *", "^[^=].*$", " *MAX *", " *MIN *",
                    " *AVERAGE *", " *: *", " *CONCAT *", " *, *", " *\"[^=].*\" *"));

    private static final List<String>                         nonterminals    = Collections
            .unmodifiableList(Arrays.asList(CELL_VALUE, STRING, FORMULA, MATH_EXPRESSION, SUM, TERM, FACTOR, UNARY_SUM,
                    MAX_RANGE, MIN_RANGE, AVERAGE_RANGE, VALUES_LIST, CONCAT_VALUES));

    private static final List<CellParseTreeInterpreterAction> actions         = Collections.unmodifiableList(
            Arrays.asList(new ReturnChildValueAction(0), new ReturnStringAction(), new ReturnChildValueAction(1),
                    new ReturnChildValueAction(0), new ReturnBinOpOnChildrenAction(), new ReturnBinOpOnChildrenAction(),
                    new ReturnFactorAction(), new ReturnUnaryOpOnChildrenAction(), new ReturnMaxAction(),
                    new ReturnMinAction(), new ReturnAverageAction(), null, new ReturnConcatAction()));

    // @formatter:off
 
    private static final String[][] productionRules = {    
        {"r1", CELL_VALUE, STRING},
        {"r2", CELL_VALUE, FORMULA},
        {"r3", STRING, STR_VALUE},
        {"r4", STRING, EQUAL},
        {"r5", FORMULA, EQUAL + " " + MATH_EXPRESSION},
        {"r6", MATH_EXPRESSION, SUM},
        {"r7", SUM, SUM + " " + PLUS + " " + TERM},
        {"r8", SUM, SUM + " " + MINUS + " " + TERM},
        {"r9", SUM, TERM},
        {"r10", TERM, TERM + " " + TIMES + " " + FACTOR},
        {"r11", TERM, TERM + " " + DIVIDE + " " + FACTOR},
        {"r12", TERM, FACTOR},
        {"r13", FACTOR, NUM_VALUE},
        {"r14", FACTOR, REF_VALUE},
        {"r15", FACTOR, OPEN_PAR + " " + MATH_EXPRESSION + " " + CLOSE_PAR},
        {"r16", FACTOR, UNARY_SUM},
        {"r17", FACTOR, MAX_RANGE},
        {"r18", FACTOR, MIN_RANGE},
        {"r19", FACTOR, AVERAGE_RANGE},
        {"r20", FACTOR, CONCAT_VALUES},        
        {"r21", UNARY_SUM, PLUS + " " + FACTOR},
        {"r22", UNARY_SUM, MINUS + " " + FACTOR},
        {"r23", MAX_RANGE, MAX + " " + OPEN_PAR + " " + REF_VALUE +  " " + RANGE + " " + REF_VALUE + " " + CLOSE_PAR},
        {"r24", MIN_RANGE, MIN + " " + OPEN_PAR + " " + REF_VALUE + " " + RANGE + " " + REF_VALUE + " " + CLOSE_PAR},
        {"r25", AVERAGE_RANGE, AVERAGE + " " + OPEN_PAR + " " + REF_VALUE + " " + RANGE + " " + REF_VALUE + " " + CLOSE_PAR},
        {"r26", VALUES_LIST, MATH_EXPRESSION},
        {"r27", VALUES_LIST, STRING_LITERAL},
        {"r28", VALUES_LIST, VALUES_LIST + " " + COMMA + " " + MATH_EXPRESSION},
        {"r29", VALUES_LIST, VALUES_LIST + " " + COMMA + " " + STRING_LITERAL},
        {"r32", CONCAT_VALUES, CONCAT + " " + OPEN_PAR + " " + VALUES_LIST + " " + CLOSE_PAR},
    };
    
    // @formatter:on

    public static List<String> getTerminals() {
        return terminals;
    }

    public static List<String> getRegex() {
        return regex;
    }

    public static List<String> getNonterminals() {
        return nonterminals;
    }

    public static List<CellParseTreeInterpreterAction> getActions() {
        return actions;
    }

    @SuppressWarnings("CPD-END")
    public static List<List<String>> getProductionRules() {
        List<List<String>> result = new ArrayList<List<String>>();
        for (String[] rule : productionRules) {
            result.add(Collections.unmodifiableList(Arrays.asList(rule)));
        }

        return Collections.unmodifiableList(result);
    }

    public static final String startSymbol = CELL_VALUE;
}
