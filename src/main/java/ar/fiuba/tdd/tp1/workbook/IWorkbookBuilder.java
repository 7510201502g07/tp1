package ar.fiuba.tdd.tp1.workbook;

import ar.fiuba.tdd.tp1.api.IWorkbook;

//Interface for building workbooks.
//WorkbookControllers should have one assigned, so as to easily 
// be able to change the implementation of books without changing
// the controller.
public interface IWorkbookBuilder {

    public IWorkbook buildWorkbook(String name);

}
