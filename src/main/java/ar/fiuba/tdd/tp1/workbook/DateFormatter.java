package ar.fiuba.tdd.tp1.workbook;

import ar.fiuba.tdd.tp1.api.IFormatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatter implements IFormatter {

    private String dateFormat = "DD-MM-YYYY";
    private String type     = "Date";
    String[] formatStrings = {"yyyy-MM-dd'T'HH:mm:ss'Z'"};
    
    public String getDateFormat() {
        return this.dateFormat;
    }
    
    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }
    
    public String toString() {
        return "\"Date.Format\": \"" + dateFormat + "\"";
    }
    
    @Override
    public String applyFormat(String value) {
        try {
            Date date = this.tryParse(value);
            SimpleDateFormat format = new SimpleDateFormat(dateFormat);
            return format.format(date).toString();
        } catch (ParseException ex) {
            return "Error:BAD_DATE";
        }
    }

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public void setNewFormat(String formatter, String format) {
        switch (formatter) {
            case "format": this.dateFormat = format; break;
            default: ;
        }
    }
    

    private Date tryParse(String dateString) throws ParseException {
        for (String formatString : formatStrings) {
            try {
                return new SimpleDateFormat(formatString).parse(dateString);
            } catch (ParseException e) {
                throw new ParseException(formatString, 0);
            }
        }
    
        return null;
    }
}
