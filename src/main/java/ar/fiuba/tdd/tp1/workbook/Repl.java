package ar.fiuba.tdd.tp1.workbook;

import ar.fiuba.tdd.tp1.api.ConcreteWorkbookControllerBuilder;
import ar.fiuba.tdd.tp1.api.DuplicateBookNameException;
import ar.fiuba.tdd.tp1.api.DuplicateSheetNameException;
import ar.fiuba.tdd.tp1.api.ISheetCell;
import ar.fiuba.tdd.tp1.api.ISpreadsheet;
import ar.fiuba.tdd.tp1.api.IWorkbook;
import ar.fiuba.tdd.tp1.api.IWorkbookController;
import ar.fiuba.tdd.tp1.api.InvalidBookNameException;
import ar.fiuba.tdd.tp1.api.InvalidCellIDException;
import ar.fiuba.tdd.tp1.api.InvalidFormulaException;
import ar.fiuba.tdd.tp1.api.InvalidSheetNameException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Repl {

    private static ConcreteWorkbookControllerBuilder builder = new ConcreteWorkbookControllerBuilder();
    private static IWorkbookController controller = builder.buildWorkbookController();

    public static void main(String[] args) {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in, "UTF-8"));
            initialSettings(in);
            displayOptions();
            String option;
            while ((option = in.readLine()) != null) {
                optionsSelection(in, option);
            }
            in.close();
        } catch (DuplicateBookNameException | IOException e) {
            e.printStackTrace();
        }
    }

    private static void optionsSelection(BufferedReader in, String option) throws IOException {
        switch (option) {
            case "1": setCellValueOption(in); break;
            case "2": getCellValueOption(in); break;
            case "3": addSheetOption(in); break;
            case "4": createBookOption(in); break;
            case "5": printProgram(); return;
            default: System.out.println("Invalid option. Please enter one from the list above"); 
        }
    }

    private static void initialSettings(BufferedReader in) throws IOException, DuplicateBookNameException {
        System.out.println("Welcome to the FiubaSpreadSheet Program\n");
        System.out.print("Enter a name for the book: ");
        String bookName = in.readLine();
        controller.addBook(bookName);
    }

    private static void displayOptions() {
        System.out.println("\nChoose between these options:");
        System.out.println("-------------------------");
        System.out.println("1 - Set value for cell");
        System.out.println("2 - Get value as double from cell");
        System.out.println("3 - Add a new sheet");
        System.out.println("4 - Create a new book");
        System.out.println("5 - Print program and Quit\n");
    }

    private static void createBookOption(BufferedReader in) {
        System.out.print("Enter a name for the new book: ");
        try {
            String bookName = in.readLine();
            controller.addBook(bookName);
            nextAction(in);
        } catch (DuplicateBookNameException | IOException e) {
            System.out.println("Book name is already use. Please enter another one");
        }
    }

    private static void addSheetOption(BufferedReader in) {
        try {
            String bookName = getBookName(in, controller.getBooks());
            IWorkbook book = controller.getBook(bookName);
            System.out.print("Enter a name for the new sheet: ");
            String sheetName = in.readLine();
            book.addSheet(sheetName);
            nextAction(in);
        } catch (IOException | DuplicateSheetNameException e) {
            System.out.println("Sheet name is already use. Please enter another one");
        } catch (InvalidBookNameException e) {
            System.out.println("Invalid book name");
        }
    }

    private static void printProgram() throws IOException {
        for (IWorkbook book : controller.getBooks()) {
            System.out.println("Book: " + book.getName());
            for (ISpreadsheet sheet : book.getSheets()) {
                System.out.println("- Sheet: " + sheet.getName());
                HashMap<String, ISheetCell> cells = sheet.getCells();
                for (Entry<String, ISheetCell> cell : cells.entrySet()) {
                    try {
                        System.out.println("  - Cell " + cell.getKey() + ": " + cell.getValue().getResult());
                    } catch (InvalidFormulaException | InvalidSheetNameException | InvalidBookNameException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private static ISheetCell getCell(BufferedReader in)
            throws IOException, InvalidBookNameException, InvalidSheetNameException, InvalidCellIDException {
        String bookName = getBookName(in, controller.getBooks());
        IWorkbook book = controller.getBook(bookName);
        String sheetName = getSheetName(in, book.getSheets());
        ISpreadsheet sheet = book.getSheet(sheetName, true);
        System.out.print("Enter the cell id: ");
        String cellId = in.readLine();
        ISheetCell cell = sheet.getCell(cellId, true);
        return cell;
    }

    private static void displayCellValue(ISheetCell cell)
            throws InvalidFormulaException, InvalidSheetNameException, InvalidBookNameException {
        System.out.println("The value of the cell is: " + cell.getResult());
    }

    private static void nextAction(BufferedReader in) throws IOException {
        if (userContinueWithOtherOption(in)) {
            displayOptions();
        } else {
            printProgram();
        }
    }

    private static boolean userContinueWithOtherOption(BufferedReader in) throws IOException {
        System.out.println("Do you want to select another option? Y/N");
        String userChoice = in.readLine();
        if (userChoice != null) {
            if (userChoice.toUpperCase().equals("Y")) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private static String getSheetName(BufferedReader in, List<ISpreadsheet> sheets) throws IOException {
        String sheetName = "";
        if (sheets.size() > 1) {
            System.out.print("Enter the name of the sheet: ");
            sheetName = in.readLine();
        } else {
            sheetName = sheets.get(0).getName();
        }
        return sheetName;
    }

    private static String getBookName(BufferedReader in, List<IWorkbook> books) throws IOException {
        String bookName = "";
        if (books.size() > 1) {
            System.out.print("Enter the name of the book: ");
            bookName = in.readLine();
        } else {
            bookName = books.get(0).getName();
        }
        return bookName;
    }

    @SuppressWarnings("CPD-START")
    private static void getCellValueOption(BufferedReader in) {
        try {
            ISheetCell cell = getCell(in);
            displayCellValue(cell);
            nextAction(in);
        } catch (InvalidSheetNameException e) {
            System.out.println("Invalid sheet name");
        } catch (InvalidCellIDException e) {
            System.out.println("Invalid cell id");
        } catch (InvalidBookNameException e) {
            System.out.println("Invalid book name");
        } catch (InvalidFormulaException | IOException e) {
            System.out.println("Invalid value");
        }
    }
    
    @SuppressWarnings("CPD-END")
    public static void setCellValueOption(BufferedReader in) {
        try {
            ISheetCell cell = getCell(in);
            System.out.print("Enter the value for the cell: ");
            String value = in.readLine();
            cell.setValue(value);
            nextAction(in);
        } catch (InvalidSheetNameException e) {
            System.out.println("Invalid sheet name");
        } catch (InvalidCellIDException e) {
            System.out.println("Invalid cell id");
        } catch (InvalidBookNameException e) {
            System.out.println("Invalid book name");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}