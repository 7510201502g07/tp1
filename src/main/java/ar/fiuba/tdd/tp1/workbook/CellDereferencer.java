package ar.fiuba.tdd.tp1.workbook;

import ar.fiuba.tdd.tp1.api.ISheetCell;
import ar.fiuba.tdd.tp1.api.IWorkbookController;
import ar.fiuba.tdd.tp1.api.InvalidBookNameException;
import ar.fiuba.tdd.tp1.api.InvalidCellIDException;
import ar.fiuba.tdd.tp1.api.InvalidSheetNameException;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// This class serves as a global access point for any cell reference.
// It can get any cell reference specified by book + sheet + cellId, or it 
// can infer the book and/or sheet by using its context (ie: the active
// book and sheet that is updated by the sheet and book observers).
public class CellDereferencer {

    private static CellDereferencer dereferencer = new CellDereferencer();

    private IWorkbookController     controller;
    private String                  activeBookName;
    private String                  activeSheetName;

    private CellDereferencer() {

        this.activeBookName = null;
        this.activeSheetName = null;
    }

    public static CellDereferencer getInstance() {
        return dereferencer;
    }

    public void setBookController(IWorkbookController controller) {
        this.controller = controller;
    }

    public void setActiveBook(String bookName) {
        this.activeBookName = bookName;
    }

    public void setActiveSheet(String sheetName) {
        this.activeSheetName = sheetName;
    }

    public ISheetCell dereference(String cellRef)
            throws InvalidCellIDException, InvalidSheetNameException, InvalidBookNameException {

        String targetBookName = getBookName(cellRef);
        String targetSheetName = getSheetName(cellRef);
        String targetCellId = getCellId(cellRef);
        return controller.getBook(targetBookName).getSheet(targetSheetName, false).getCell(targetCellId, false);
    }

    public List<ISheetCell> dereferenceRange(String startOfRangeCellRef, String endOfRangeCellRef)
            throws InvalidCellIDException, InvalidSheetNameException, InvalidBookNameException {

        List<ISheetCell> cells = new ArrayList<>();

        String targetBookName = getBookName(startOfRangeCellRef);
        String targetSheetName = getSheetName(startOfRangeCellRef);

        String startColumn = getColumn(startOfRangeCellRef);
        String endColumn = advanceColumn(getColumn(endOfRangeCellRef));
        int startRow = getRow(startOfRangeCellRef);
        int endRow = getRow(endOfRangeCellRef);

        for (String currentColumn = startColumn; !currentColumn
                .equals(endColumn); currentColumn = advanceColumn(currentColumn)) {
            for (int currentRow = startRow; currentRow <= endRow; currentRow++) {
                cells.add((controller.getBook(targetBookName).getSheet(targetSheetName, false)
                        .getCell(currentColumn + currentRow, false)));
            }
        }

        return cells;
    }

    private String getColumn(String cellRef) {

        Pattern regexPattern = Pattern.compile("[A-Z]+");
        Matcher matcher = regexPattern.matcher(cellRef);
        matcher.find();
        return matcher.group();
    }

    private int getRow(String cellRef) {
        Pattern regexPattern = Pattern.compile("[1-9][0-9]*");
        Matcher matcher = regexPattern.matcher(cellRef);
        matcher.find();
        return Integer.parseInt(matcher.group());
    }

    private String advanceColumn(String currentColumn) {

        int count = 0;
        char currentChar = 'Z';

        while (currentChar == 'Z' && count < currentColumn.length()) {
            count++;
            currentChar = currentColumn.charAt(currentColumn.length() - count);
        }

        String nextColumn = buildNextColumn(count, currentColumn);

        return nextColumn;
    }

    private String buildNextColumn(int count, String currentColumn) {

        String nextColumn = "";
        char currentChar = currentColumn.charAt(currentColumn.length() - count);

        if (count == currentColumn.length() && currentChar == 'Z') {
            nextColumn += "A";
        } else {
            String aux = Character.toString((char) (((int) currentColumn.charAt(currentColumn.length() - count)) + 1));
            nextColumn += currentColumn.substring(0, currentColumn.length() - count).concat(aux);
        }

        StringBuilder bld = new StringBuilder();
        for (int j = 0; j < count - 1; j++) {
            bld.append("A");
        }
        String str = bld.toString();

        return nextColumn.concat(str);
    }

    private String getBookName(String cellRef) {
        Pattern regexPattern = Pattern.compile("^[a-zA-Z_0-9]+\\]");
        Matcher matcher = regexPattern.matcher(cellRef);
        if (matcher.find()) {
            return matcher.group().split("]")[0];
        } else {
            return this.activeBookName;
        }
    }

    private String getSheetName(String cellRef) {

        Pattern regexPattern = Pattern.compile("[a-zA-Z_0-9]+!");
        Matcher matcher = regexPattern.matcher(cellRef);
        if (matcher.find()) {
            return matcher.group().split("!")[0];
        } else {
            return this.activeSheetName;
        }
    }

    private String getCellId(String cellRef) {

        Pattern regexPattern = Pattern.compile("[A-Z]+[1-9][0-9]*$");
        Matcher matcher = regexPattern.matcher(cellRef);
        matcher.find();
        return matcher.group();
    }
}
