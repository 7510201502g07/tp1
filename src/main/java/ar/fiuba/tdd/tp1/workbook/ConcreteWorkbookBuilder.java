package ar.fiuba.tdd.tp1.workbook;

import ar.fiuba.tdd.tp1.api.DuplicateSheetNameException;
import ar.fiuba.tdd.tp1.api.IWorkbook;
import ar.fiuba.tdd.tp1.undoredo.UndoRedo;

public class ConcreteWorkbookBuilder implements IWorkbookBuilder {

    UndoRedo changesTracker;

    public ConcreteWorkbookBuilder(UndoRedo changesTracker) {
        this.changesTracker = changesTracker;
    }

    @Override
    public IWorkbook buildWorkbook(String name) {

        IWorkbookElementBuilder elementBuilder = new ConcreteWorkbookElementBuilder(changesTracker);
        ConcreteWorkbook newWorkbook = new ConcreteWorkbook(name, elementBuilder);
        try {
            newWorkbook.addSheet("default");
        } catch (DuplicateSheetNameException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        BookFocusObserver bookObserver = new BookFocusObserver(newWorkbook, changesTracker);
        newWorkbook.attach(bookObserver);
        return newWorkbook;
    }

}
