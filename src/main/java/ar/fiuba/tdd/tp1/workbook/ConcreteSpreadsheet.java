package ar.fiuba.tdd.tp1.workbook;

import ar.fiuba.tdd.tp1.api.ISheetCell;
import ar.fiuba.tdd.tp1.api.ISpreadsheet;
import ar.fiuba.tdd.tp1.api.InvalidCellIDException;
import ar.fiuba.tdd.tp1.observerpattern.IObservable;
import ar.fiuba.tdd.tp1.observerpattern.IObserver;
import ar.fiuba.tdd.tp1.observerpattern.IObserver.ObservableEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ConcreteSpreadsheet implements ISpreadsheet, IObservable {

    private String                      name;
    private IWorkbookElementBuilder     builder;
    private HashMap<String, ISheetCell> cells;
    private List<IObserver>              observers;

    public ConcreteSpreadsheet(String name, IWorkbookElementBuilder builder) {
        this.name = name;
        this.builder = builder;
        this.cells = new HashMap<>();
        this.observers = new ArrayList<>();
    }

    public String getName() {
        return this.name;
    }

    @Override
    public ISheetCell getCell(String id, boolean pullFocus) throws InvalidCellIDException {

        if (pullFocus) {
            notifyObservers(IObserver.ObservableEvent.SHEET_FOCUS_CHANGE);
        }

        testCellId(id);

        ISheetCell requiredCell = this.cells.get(id);
        
        if (requiredCell == null) {
            requiredCell = builder.buildCell(id);
            this.cells.put(id, requiredCell);
        }
        
        return requiredCell;
    }

    @Override
    public ISheetCell getCell(String column, int row) throws InvalidCellIDException {
        return getCell(column + row, true);
    }

    private void testCellId(String id) throws InvalidCellIDException {
        if (!id.matches("[A-Z]+[1-9][0-9]*")) {
            throw new InvalidCellIDException();
        }
    }

    @Override
    public void notifyObservers(ObservableEvent event) {
        for (IObserver obs : this.observers) {
            obs.update(event);
        }
    }

    @Override
    public void attach(IObserver observer) {
        this.observers.add(observer);
    }

    @Override
    public void detach(IObserver observer) {
        this.observers.remove(observer);
    }
    
    public HashMap<String, ISheetCell> getCells() {
        return cells;
    }
    
    public List<ISheetCell> getListCells() {
        List<ISheetCell> listCells = new ArrayList<ISheetCell>();
        for (String id : this.cells.keySet()) {
            ISheetCell cell = null;
            try {
                cell = this.getCell(id, true);
            } catch (InvalidCellIDException e) {
                e.printStackTrace();
            }
            listCells.add(cell);
        }
        return listCells;
    }
    
    public void setCells(HashMap<String, ISheetCell> cells) {
        this.cells = cells;
    }
}
