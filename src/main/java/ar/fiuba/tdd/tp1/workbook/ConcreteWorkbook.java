package ar.fiuba.tdd.tp1.workbook;

import ar.fiuba.tdd.tp1.api.DuplicateSheetNameException;
import ar.fiuba.tdd.tp1.api.ISpreadsheet;
import ar.fiuba.tdd.tp1.api.IWorkbook;
import ar.fiuba.tdd.tp1.api.InvalidSheetNameException;
import ar.fiuba.tdd.tp1.observerpattern.IObservable;
import ar.fiuba.tdd.tp1.observerpattern.IObserver;
import ar.fiuba.tdd.tp1.observerpattern.IObserver.ObservableEvent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ConcreteWorkbook implements IWorkbook, IObservable {

    private List<ISpreadsheet>      sheets;
    private String                  name;
    private String                  version;

    private List<IObserver>         observers;
    private IWorkbookElementBuilder builder;
    private ISpreadsheet            changedSheet;

    public ConcreteWorkbook(String name, IWorkbookElementBuilder builder) {
        this.name = name;
        this.builder = builder;
        this.sheets = new ArrayList<>();
        this.observers = new ArrayList<>();
        this.version = "1.0";
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void addSheet(String name) throws DuplicateSheetNameException {

        notifyObservers(IObserver.ObservableEvent.BOOK_FOCUS_CHANGE);

        if (findSheet(name) != null) {
            throw new DuplicateSheetNameException();
        }

        ISpreadsheet newSheet = this.builder.buildSheet(name);
        this.sheets.add(newSheet);

        this.changedSheet = newSheet;
        notifyObservers(IObserver.ObservableEvent.SHEET_ADDED);
    }

    public void addSheet(ISpreadsheet sheet) {
        this.sheets.add(sheet);
    }

    @Override
    public void removeSheet(String name) throws InvalidSheetNameException {

        notifyObservers(IObserver.ObservableEvent.BOOK_FOCUS_CHANGE);

        ISpreadsheet sheetToRemove = findSheet(name);

        if (sheetToRemove == null) {
            throw new InvalidSheetNameException();
        }

        this.sheets.remove(sheetToRemove);

        this.changedSheet = sheetToRemove;
        notifyObservers(IObserver.ObservableEvent.SHEET_REMOVED);
    }

    public void removeSheet(ISpreadsheet sheet) {
        this.sheets.remove(sheet);
    }

    @Override
    public ISpreadsheet getSheet(String name, boolean pullFocus) throws InvalidSheetNameException {

        if (pullFocus) {
            notifyObservers(IObserver.ObservableEvent.BOOK_FOCUS_CHANGE);
        }

        ISpreadsheet sheetToGet = findSheet(name);

        if (sheetToGet == null) {
            throw new InvalidSheetNameException();
        }

        return sheetToGet;
    }

    @Override
    public List<ISpreadsheet> getSheets() {

        notifyObservers(IObserver.ObservableEvent.BOOK_FOCUS_CHANGE);

        return Collections.unmodifiableList(this.sheets);
    }

    @Override
    public void detach(IObserver observer) {
        this.observers.remove(observer);
    }

    @Override
    public void attach(IObserver observer) {
        this.observers.add(observer);
    }

    @Override
    public void notifyObservers(ObservableEvent event) {
        for (IObserver obs : this.observers) {
            obs.update(event);
        }
    }

    private ISpreadsheet findSheet(String name) {
        for (ISpreadsheet sheet : this.sheets) {
            if (sheet.getName().equals(name)) {
                return sheet;
            }
        }
        return null;
    }

    public ISpreadsheet getChangedSheet() {
        return this.changedSheet;
    }

    @Override
    public String getVersion() {
        return this.version;
    }

    @Override
    public void setVersion(String version) {
        this.version = version;
    }
}
