package ar.fiuba.tdd.tp1.workbook;

import ar.fiuba.tdd.tp1.api.IFormatter;

public class StringFormatter implements IFormatter {
    
    private String type     = "String";
    private String format   = "bold";
    
    @Override
    public String applyFormat(String value) {
        return value;
    }
    
    public void setStringFormat(String format) {
        this.format = format;
    }

    public String getStringFormat() {
        return this.format;
    }
    
    @Override
    public String getType() {
        return this.type;
    }
    
    public String toString() {
        return "\"String.Format\": \"" + format + "\"";
    }

    @Override
    public void setNewFormat(String formatter, String format) {
        switch (formatter) {
            case "format": this.format = format; break;
            default: System.out.println("format '" + format + "undefined");
        }
    }
}
