package ar.fiuba.tdd.tp1.workbook;

import ar.fiuba.tdd.tp1.api.ICycleDetector;
import ar.fiuba.tdd.tp1.api.ISheetCell;

import org.jgrapht.alg.CycleDetector;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DetectCycle implements ICycleDetector{
    
    private DefaultDirectedGraph<String, DefaultEdge> cellGraph;
    private CycleDetector<String, DefaultEdge> cycleDetector;
    
    public DetectCycle() {
        this.cellGraph = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);
        this.cycleDetector = new CycleDetector<String, DefaultEdge>(cellGraph);
    }
    
    private void addCellId(String cellId) {
        this.cellGraph.addVertex(cellId);
    }
    
    private void addCellEdges(String cellId, String cellRawValue) {
        if (containsConcreteCell(cellRawValue)) {
            String concreteCell = obtainConcreteCellFromRawValue(cellRawValue);
            if (!isGraphVertex(concreteCell)) {
                addCellId(concreteCell);
            }
            this.cellGraph.addEdge(cellId, concreteCell);
        }
    }
    
    @Override
    public void addCells(HashMap<String, ISheetCell> cells) {
        for (Entry<String, ISheetCell> cell : cells.entrySet()) {
            if (!isGraphVertex(cell.getKey())) {
                addCellId(cell.getKey());
            }
            addCellEdges(cell.getKey(), cell.getValue().getRawValue());
        }
    }
    
    @Override
    public boolean hasCycle(String cellId) {
        if (this.cycleDetector.detectCycles()) {
            Set<String> cycleVertices;
            cycleVertices = this.cycleDetector.findCycles();
            if (cycleVertices.contains(cellId)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
    
    private boolean isGraphVertex(String rawValue) {
        return this.cellGraph.containsVertex(rawValue);
    }
    
    private boolean containsConcreteCell(String rawValue) {
        return rawValue.matches("(.*)([a-zA-Z][0-9])(.*)");
    }
    
    private String obtainConcreteCellFromRawValue(String rawValue) {
        Pattern pattern = Pattern.compile("[a-zA-Z][0-9]");
        Matcher matcher = pattern.matcher(rawValue);
        if (matcher.find()) {
            return matcher.group(0);
        } else {
            return "";
        }
    }
}

    

