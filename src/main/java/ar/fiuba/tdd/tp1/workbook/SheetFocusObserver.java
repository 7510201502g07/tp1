package ar.fiuba.tdd.tp1.workbook;

import ar.fiuba.tdd.tp1.api.ISpreadsheet;
import ar.fiuba.tdd.tp1.observerpattern.IObserver;

// Observes a sheet, watching for it to get focus (ie: become the active sheet).
// When this happens, it changes the active sheet in CellDereferencer.
public class SheetFocusObserver implements IObserver {

    ISpreadsheet observedSheet;

    public SheetFocusObserver(ConcreteSpreadsheet observedSheet) {
        this.observedSheet = observedSheet;
    }

    @Override
    public void update(ObservableEvent event) {
        if (event != IObserver.ObservableEvent.SHEET_FOCUS_CHANGE) {
            return;
        }

        CellDereferencer.getInstance().setActiveSheet(this.observedSheet.getName());
    }

}
