package ar.fiuba.tdd.tp1.contextfreegrammar;

import ar.fiuba.tdd.tp1.lexer.Token;
import ar.fiuba.tdd.tp1.parser.ConcreteParseTree;
import ar.fiuba.tdd.tp1.parser.ParseTree;

import java.util.Arrays;
import java.util.Deque;
import java.util.List;
import java.util.NoSuchElementException;

public class LR1ProductionRule {

    String       name;
    String       result;
    List<String> rule;

    public LR1ProductionRule(String name, String result, String[] rule) {
        this.name = name;
        this.result = result;
        this.rule = Arrays.asList(rule);
    }

    public int length() {
        return this.rule.size();
    }

    public String getSymbol(int position) throws Exception {
        if (position >= this.rule.size()) {
            throw new Exception("Can't get symbol. Reason: invalid position.");
        }
        return this.rule.get(position);
    }

    public String getResult() {
        return this.result;
    }

    public void apply(Deque<String> stateStack, Deque<ParseTree> parseStack) throws Exception {

        ParseTree newTree = new ConcreteParseTree();

        Token node = new Token(this.result, "");
        newTree.setNode(node);

        for (int i = rule.size() - 1; i >= 0; i--) {
            try {
                ParseTree tempTree = parseStack.pop();
                stateStack.pop();

                if (!tempTree.getNode().getType().equals(this.rule.get(i))) {
                    throw new Exception("Error when trying to apply rule: expected \"" + this.rule.get(i)
                            + "\", but found \"" + tempTree.getNode().getType() + "\" instead.");
                }

                tempTree.setParent(newTree);
                newTree.addChild(tempTree);
            } catch (NoSuchElementException e) {
                throw new Exception("Not enough components to apply rule: expected " + this.rule.size() + ".");
            }
        }

        parseStack.push(newTree);
    }

    @Override
    public int hashCode() {
        return this.name.hashCode() + this.result.hashCode();

    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (obj == null || !obj.getClass().equals(this.getClass())) {
            return false;
        }

        LR1ProductionRule other = (LR1ProductionRule) obj;

        return this.name.equals(other.name) || (this.result.equals(other.result) && this.rule.equals(other.rule));
    }
}
