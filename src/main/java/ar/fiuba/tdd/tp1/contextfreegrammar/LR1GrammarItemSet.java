package ar.fiuba.tdd.tp1.contextfreegrammar;

import java.util.HashSet;
import java.util.Set;

public class LR1GrammarItemSet {

    static int                 instanceCount = 0;
    String                     id;
    ExtendedContextFreeGrammar grammar;
    Set<LR1GrammarItem>        items;

    private static String getId() {
        return Float.toString(instanceCount++);
    }

    public LR1GrammarItemSet(ExtendedContextFreeGrammar grammar) {
        this.id = getId();
        this.grammar = grammar;
        this.items = new HashSet<LR1GrammarItem>();
    }

    public boolean addLR1GrammarItem(LR1GrammarItem item) {
        return this.items.add(item);
    }

    public void closeSet() throws Exception {

        boolean itemsAdded = true;
        while (itemsAdded) {
            itemsAdded = addNewItems();
        }
    }

    private boolean addNewItems() throws Exception {

        Set<LR1GrammarItem> tempItems = new HashSet<LR1GrammarItem>();
        boolean itemsAdded = false;
        for (LR1GrammarItem item : this.items) {

            String nextSymbol = item.getNextSymbol();
            if (nextSymbol == null || grammar.isTerminalSymbol(nextSymbol)) {
                continue;
            }

            tempItems.addAll(getPotentialItems(item, nextSymbol));
        }

        if (this.items.addAll(tempItems)) {
            itemsAdded = true;
        }
        return itemsAdded;
    }

    private Set<LR1GrammarItem> getPotentialItems(LR1GrammarItem item, String nextSymbol) throws Exception {
        Set<LR1GrammarItem> potentialItems = new HashSet<LR1GrammarItem>();
        for (LR1ProductionRule rule : grammar.getRulesFor(nextSymbol)) {
            String rest = item.getNextItem().getNextSymbol();
            if (rest == null) {
                rest = item.lookahead;
            }
            // TODO: check this exception.
            Set<String> firstSet = grammar.calculateFirstSet(rest);
            for (String symbol : firstSet) {
                LR1GrammarItem newItem = new LR1GrammarItem(rule, symbol);
                potentialItems.add(newItem);
            }
        }
        return potentialItems;
    }

    public LR1GrammarItemSet gotoSymbol(String symbol) throws Exception {

        // TODO: check this.
        LR1GrammarItemSet newItemSet = new LR1GrammarItemSet(this.grammar);

        for (LR1GrammarItem item : this.items) {
            if (item.getNextSymbol() != null && item.getNextSymbol().equals(symbol)) {
                LR1GrammarItem nextItem = item.getNextItem();
                if (nextItem != null) {
                    newItemSet.addLR1GrammarItem(nextItem);
                }
            }
        }

        newItemSet.closeSet();
        return newItemSet;
    }

    public Set<String> getNextSymbols() {

        Set<String> nextSymbols = new HashSet<String>();

        for (LR1GrammarItem item : this.items) {
            String nextSymbol = item.getNextSymbol();
            if (nextSymbol == null) {
                continue;
            }
            nextSymbols.add(nextSymbol);
        }

        return nextSymbols;

    }

    @Override
    public int hashCode() {
        return this.items.hashCode();
    }

    private boolean equalsGrammarItemSet(LR1GrammarItemSet other) {
        return this.items.equals(other.items);
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == null) {
            return false;
        }

        if (!this.getClass().equals(obj.getClass())) {
            return false;
        }

        return this.equalsGrammarItemSet((LR1GrammarItemSet) obj);
    }
}
