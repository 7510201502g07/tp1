package ar.fiuba.tdd.tp1.contextfreegrammar;

import ar.fiuba.tdd.tp1.api.InvalidParseStateException;
import ar.fiuba.tdd.tp1.parser.DoneParseAction;
import ar.fiuba.tdd.tp1.parser.LR1ParseAction;
import ar.fiuba.tdd.tp1.parser.ReduceParseAction;
import ar.fiuba.tdd.tp1.parser.ShiftParseAction;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class LR1ParseTable {

    ExtendedContextFreeGrammar  grammar;
    Map<String, LR1ParseAction> stateTable;
    Map<String, String>         gotoTable;
    Set<String>                 registeredStates;
    String                      startState;

    public LR1ParseTable(ExtendedContextFreeGrammar grammar) {
        this.grammar = grammar;
        stateTable = new HashMap<String, LR1ParseAction>();
        gotoTable = new HashMap<String, String>();
        registeredStates = new HashSet<String>();
        startState = null;
    }

    public void registerState(String state) {

        this.registeredStates.add(state);
    }

    public void registerStateTableEntry(String state, String lookahead, String entry) throws Exception {

        testState(state);
        testTerminalSymbol(lookahead);

        LR1ParseAction parseAction = null;

        if (entry.equals("DONE")) {
            parseAction = new DoneParseAction();
        } else if (registeredStates.contains(entry)) {
            parseAction = new ShiftParseAction(entry);
        } else if (grammar.isProductionRule(entry)) {
            parseAction = new ReduceParseAction(grammar.getProductionRule(entry));
        } else {
            throw new Exception("Unrecognized entry: \"" + entry + "\".");
        }

        stateTable.put(state + lookahead, parseAction);
    }

    public void registerGotoTableEntry(String state, String symbol, String nextState) throws Exception {

        testState(state);
        testNonterminalSymbol(symbol);
        testState(nextState);

        gotoTable.put(state + symbol, nextState);
    }

    public void registerStartState(String state) throws Exception {
        testState(state);
        testStartState();

        this.startState = state;
    }

    public LR1ParseAction getNextAction(String currentState, String lookahead) throws Exception {

        testState(currentState);
        testTerminalSymbol(lookahead);

        LR1ParseAction nextAction = stateTable.get(currentState + lookahead);

        if (nextAction == null) {
            throw new InvalidParseStateException();
        }

        return nextAction;
    }

    public String getNextState(String currentState, String symbol) throws Exception {

        testState(currentState);
        testNonterminalSymbol(symbol);

        String nextState = gotoTable.get(currentState + symbol);

        if (nextState == null) {
            throw new Exception(
                    "No goto table entry for reduction \"" + symbol + "\" to state\"" + currentState + "\".");
        }

        return nextState;
    }

    private void testState(String state) throws Exception {
        if (!this.registeredStates.contains(state)) {
            throw new Exception("Unrecognized state: \"" + state + "\".");
        }
    }

    private void testTerminalSymbol(String symbol) throws Exception {
        if (!grammar.isTerminalSymbol(symbol)) {
            throw new Exception("Unrecognized terminal symbol: \"" + symbol + "\".");
        }
    }

    private void testNonterminalSymbol(String symbol) throws Exception {
        if (!grammar.isNonterminalSymbol(symbol)) {
            throw new Exception("Unrecognized nonterminal symbol: \"" + symbol + "\".");
        }
    }

    private void testStartState() throws Exception {
        if (this.startState != null) {
            throw new Exception("Can't register start state. Reason: start state already registered..");
        }
    }

    public String getStartState() {
        return this.startState;
    }
}
