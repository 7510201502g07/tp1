package ar.fiuba.tdd.tp1.undoredo;

import ar.fiuba.tdd.tp1.api.ISheetCell;

// CellChange acts as Command pattern's ConcreteCommand and Cell
// acts as Command pattern's Receiver.
public class CellChange implements IUndoable {

    private ISheetCell   cell;
    private String oldValue;
    private String newValue;

    public CellChange(ISheetCell cell, String oldValue, String newValue) {
        this.cell = cell;
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    @Override
    public void revert() {
        try {
            this.cell.setValue(this.oldValue);
        } catch (Exception e) {
            // This code shouldn't be reached.
            e.printStackTrace();
        }
    }

    @Override
    public void apply() {
        try {
            this.cell.setValue(this.newValue);
        } catch (Exception e) {
            // This code shouldn't be reached.
            e.printStackTrace();
        }
    }

}
