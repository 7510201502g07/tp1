package ar.fiuba.tdd.tp1.undoredo;

// IUndoable acts as Command pattern's ICommand
public interface IUndoable {

    public void revert();

    public void apply();
}
