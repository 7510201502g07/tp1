package ar.fiuba.tdd.tp1.observerpattern;

public interface IObserver {

    public enum ObservableEvent {
        CELL_CHANGE, SHEET_FOCUS_CHANGE, BOOK_FOCUS_CHANGE, SHEET_ADDED, SHEET_REMOVED
    }

    public void update(ObservableEvent event);

}
