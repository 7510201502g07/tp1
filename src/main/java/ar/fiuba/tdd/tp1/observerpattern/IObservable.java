package ar.fiuba.tdd.tp1.observerpattern;

import ar.fiuba.tdd.tp1.observerpattern.IObserver.ObservableEvent;

public interface IObservable {

    public void attach(IObserver observer);

    public void detach(IObserver observer);

    public void notifyObservers(ObservableEvent event);

}
