package ar.fiuba.tdd.tp1.importexport;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ar.fiuba.tdd.tp1.api.IFormatter;
import ar.fiuba.tdd.tp1.workbook.DateFormatter;
import ar.fiuba.tdd.tp1.workbook.NumberFormatter;

public class JsonNumberFormatter extends JsonFormatter {

    @Expose @SerializedName("Number.Decimal") private int decimals = 0;
    
    public JsonNumberFormatter() {}
    
    public JsonNumberFormatter(IFormatter formatter) {
        NumberFormatter numberFormatter = (NumberFormatter) formatter;
        this.decimals = numberFormatter.getDecimals();
    }  
    
    public int getDecimals() {
        return this.decimals;
    }
    
    public String toString() {
        return "numberFormatter";
    }
    
    @Override
    public void copyFrom(Object desjonizedObject) {
        JsonNumberFormatter jsonNumberFormatter = (JsonNumberFormatter) desjonizedObject;
        this.decimals = jsonNumberFormatter.getDecimals();
    }

    @Override
    public IFormatter getIFormatter() {
        NumberFormatter numberFormatter = new NumberFormatter();
        numberFormatter.setDecimals(this.decimals);
        return numberFormatter;
    }
}
