package ar.fiuba.tdd.tp1.importexport;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Jsonizer {

    private Jsonizer() {}
    
    private static Gson gson = null;
    
    public static void init() {
        gson = new GsonBuilder()
                        .excludeFieldsWithoutExposeAnnotation()
                        .setPrettyPrinting()
                        .serializeNulls()
                        .disableHtmlEscaping()
                        .registerTypeAdapter(JsonFormatter.class, new JsonFormatterAdapter<JsonFormatter>() )
                        .create();
    }
    
    public static String jsonize(IJsonable object) {
        initIfNotInitialized();
        return gson.toJson(object);
    }
    
    public static Object desjsonize(String json, Object object) {
        initIfNotInitialized();
        return gson.fromJson(json, object.getClass());
    }
    
    private static void initIfNotInitialized() {
        if (gson == null) {
            init();
        }
    }
}
