package ar.fiuba.tdd.tp1.importexport;

import ar.fiuba.tdd.tp1.api.IFormatter;

public abstract class JsonFormatter extends Jsonable {
    
    public abstract void copyFrom(Object desjonizedObject);
    
    public abstract IFormatter getIFormatter();

}
