package ar.fiuba.tdd.tp1.importexport;

import com.google.gson.annotations.Expose;

import ar.fiuba.tdd.tp1.api.ConcreteWorkbookControllerBuilder;
import ar.fiuba.tdd.tp1.api.DuplicateBookNameException;
import ar.fiuba.tdd.tp1.api.DuplicateSheetNameException;
import ar.fiuba.tdd.tp1.api.ISheetCell;
import ar.fiuba.tdd.tp1.api.ISpreadsheet;
import ar.fiuba.tdd.tp1.api.IWorkbook;
import ar.fiuba.tdd.tp1.api.IWorkbookController;
import ar.fiuba.tdd.tp1.api.IWorkbookControllerBuilder;
import ar.fiuba.tdd.tp1.api.InvalidBookNameException;
import ar.fiuba.tdd.tp1.api.InvalidCellIDException;
import ar.fiuba.tdd.tp1.api.InvalidSheetNameException;
import ar.fiuba.tdd.tp1.workbook.ConcreteSpreadsheet;

import java.util.ArrayList;
import java.util.List;

public class JsonBook extends Jsonable {

    @Expose private String name = null;
    @Expose private String version = null;
    @Expose List<JsonCell> cells = null;
    
    public JsonBook() {}
    
    public JsonBook(IWorkbook book) {
        this.name = book.getName();
        this.version = book.getVersion();
        this.cells = new ArrayList<JsonCell>();
        
        List<ISpreadsheet> sheets = book.getSheets();
        for (ISpreadsheet spreadSheet : sheets) {
            ConcreteSpreadsheet concreteSheet = (ConcreteSpreadsheet) spreadSheet;
            for (ISheetCell cell : concreteSheet.getCells().values()) {
                JsonCell jsoncell = new JsonCell(cell);
                jsoncell.setSheetName(concreteSheet.getName());
                this.cells.add( jsoncell );
            }
        }
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getVersion() {
        return this.version;
    }
    
    public List<JsonCell> getCells() {
        return cells;
    }
    
    @Override
    public void copyFrom(Object desjonizedObject) {
        JsonBook desjsonizedBook = (JsonBook) desjonizedObject;
        this.name = desjsonizedBook.getName();
        this.version = desjsonizedBook.getVersion();
        this.cells = desjsonizedBook.getCells();
    }
    
    public IWorkbook getBook() {
        IWorkbookController        controller;
        IWorkbookControllerBuilder builder = new ConcreteWorkbookControllerBuilder();
        controller = builder.buildWorkbookController();
        
        try {
            controller.addBook(this.name);
        } catch (DuplicateBookNameException e) {
            //e.printStackTrace();
        }
        
        this.addSheetsWithCellsToWorkbook(controller);
        
        IWorkbook book = null;
        
        try {
            book = controller.getBook(this.name);
        } catch (InvalidBookNameException e) {
            //e.printStackTrace();
        }
        
        return book;
    }

    public void addSheetsWithCellsToWorkbook(IWorkbookController controller) {
        
        IWorkbook book = getWorkbookInitialized(controller);
        
        ISpreadsheet sheet = null;
        ISheetCell cell = null;
        for (JsonCell jsoncell : this.cells) {
            try {
                sheet = book.getSheet(jsoncell.getSheetName(), true);
                cell = sheet.getCell(jsoncell.getId(), true);
                cell.setValue(jsoncell.getValue());
                cell.setFormatter(jsoncell.getJsonFormatter().getIFormatter());
            } catch (InvalidSheetNameException e) {
                //e.printStackTrace();
            } catch (InvalidCellIDException e) {
                //e.printStackTrace();
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }
    }
    
    public IWorkbook getWorkbookInitialized(IWorkbookController controller) {
        IWorkbook book = null;
        try {
            book = controller.getBook(this.name);
        } catch (InvalidBookNameException e) {
            e.printStackTrace();
        }
        
        for (JsonCell jsoncell : this.cells) {
            try {
                book.addSheet(jsoncell.getSheetName());
            } catch (DuplicateSheetNameException e) {
              //e.printStackTrace();
            }
        }
        return book;
    }
}
