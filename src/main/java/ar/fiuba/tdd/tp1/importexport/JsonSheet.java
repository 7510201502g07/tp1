package ar.fiuba.tdd.tp1.importexport;

import com.google.gson.annotations.Expose;

import ar.fiuba.tdd.tp1.api.ISheetCell;
import ar.fiuba.tdd.tp1.api.ISpreadsheet;
import ar.fiuba.tdd.tp1.workbook.ConcreteSpreadsheet;

import java.util.ArrayList;
import java.util.List;

public class JsonSheet extends Jsonable {
    
    @Expose List<JsonCell> cells = null;
    
    public JsonSheet() {}
    
    public JsonSheet(ISpreadsheet sheet) {
        ConcreteSpreadsheet concreteSheet = (ConcreteSpreadsheet) sheet;
        this.cells = new ArrayList<JsonCell>();
        for (ISheetCell cell : concreteSheet.getCells().values()) {
            JsonCell jsoncell = new JsonCell(cell);
            jsoncell.setSheetName(concreteSheet.getName());
            this.cells.add( jsoncell );
        }
    }
    
    public List<JsonCell> getCells() {
        return cells;
    }
    
    @Override
    public void copyFrom(Object desjonizedObject) {
        JsonSheet desjsonizedSheet = (JsonSheet) desjonizedObject;
        this.cells = desjsonizedSheet.getCells();
    }

}
