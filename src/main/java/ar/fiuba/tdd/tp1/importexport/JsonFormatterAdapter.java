package ar.fiuba.tdd.tp1.importexport;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

final class JsonFormatterAdapter<T> implements JsonSerializer<T>, JsonDeserializer<T> {
    
    private List<String> formatsList = new ArrayList<String>();
    
    public JsonFormatterAdapter() {
        formatsList.add("Number");
        formatsList.add("Date");
        formatsList.add("Money");
        formatsList.add("String");
    }
    
    public JsonElement serialize(T object, Type interfaceType, JsonSerializationContext context) {

        return context.serialize(object);
    }

    public T deserialize(JsonElement elem, Type interfaceType, JsonDeserializationContext context) throws JsonParseException {
        JsonObject object = (JsonObject) elem;
        Type concreteClass = getConcreteClass(object);
        
        return context.deserialize(elem, concreteClass);
    }

    private Type getConcreteClass(JsonObject object) {
        for (String format : formatsList) {
            if ( isFormatJsonFormatter(object, format) ) {
                return getJsonFormatterClass(format);
            }
        }
        return getJsonFormatterClass("Null");
        //throw new JsonParseException(new ClassNotFoundException());
    }   
    
    private boolean isFormatJsonFormatter(JsonObject object, String format) {
        for (Map.Entry<String,JsonElement> entry : object.entrySet()) {
            String attribute = entry.getKey();
            if ( attribute.startsWith(format + ".") == false ) {
                return false;
            }
        }
        return true;
    }
    
    private Type getJsonFormatterClass(String format) {
        try {
            return Class.forName("ar.fiuba.tdd.tp1.importexport.Json" + format + "Formatter");
        } catch (ClassNotFoundException e) {
            throw new JsonParseException(e);
        }
    }
}
