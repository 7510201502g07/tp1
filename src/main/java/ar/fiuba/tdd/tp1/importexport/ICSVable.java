package ar.fiuba.tdd.tp1.importexport;

public interface ICSVable {
    
    public String toCSV();
    
    public void fromCSV(String csv);
}
