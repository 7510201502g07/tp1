package ar.fiuba.tdd.tp1.importexport;

import com.google.gson.annotations.Expose;

import ar.fiuba.tdd.tp1.api.IFormatter;
import ar.fiuba.tdd.tp1.api.ISheetCell;
import ar.fiuba.tdd.tp1.workbook.ConcreteCell;

public class JsonCell extends Jsonable {

    @Expose private String sheet = "default";
    @Expose private String id;
    @Expose private String value;
    @Expose private String type;
    @Expose private JsonFormatter formatter;
    
    public JsonCell() {}
    
    public JsonCell(ISheetCell cell) {
        this.id = cell.getId();
        this.value = cell.getRawValue();
        this.type = cell.getFormatter().getType();
        this.formatter = this.createJsonFormatter(cell.getFormatter());
    }
    
    @Override
    public void copyFrom(Object desjonizedObject) {
        JsonCell cell = (JsonCell) desjonizedObject;
        this.id = cell.getId();
        this.value = cell.getValue();
        this.type = cell.getType();
        this.formatter = cell.getJsonFormatter();
        
    }
    
    public String getSheetName() {
        return this.sheet;
    }
    
    public void setSheetName(String sheetName) {
        this.sheet = sheetName;
    }
    
    public String getId() {
        return this.id;
    }
    
    public String getValue() {
        return this.value;
    }

    public String getType() {
        return this.type;
    }
    
    public JsonFormatter getJsonFormatter() {
        return this.formatter;
    }
    
    public JsonFormatter createJsonFormatter(IFormatter formatter) {
        
        switch (formatter.getType()) {
            case "Number": return new JsonNumberFormatter(formatter); 
            case "Date": return new JsonDateFormatter(formatter);
            case "Money": return new JsonMoneyFormatter(formatter);
            case "String": return new JsonStringFormatter(formatter);
            default: return null;
        }
    }
    
    public ISheetCell getCell() {
        ISheetCell cell = new ConcreteCell(this.id);
        try {
            cell.setValue(this.value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        cell.setFormatter(this.formatter.getIFormatter());
        return cell;
    }
}
