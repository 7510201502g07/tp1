package ar.fiuba.tdd.tp1.importexport;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ar.fiuba.tdd.tp1.api.IFormatter;
import ar.fiuba.tdd.tp1.workbook.DateFormatter;
import ar.fiuba.tdd.tp1.workbook.StringFormatter;

public class JsonStringFormatter extends JsonFormatter {

    @Expose @SerializedName("String.Format") private String stringFormat = null;
    
    public JsonStringFormatter(IFormatter formatter) {
        StringFormatter stringFormatter = (StringFormatter) formatter;
        this.stringFormat = stringFormatter.getStringFormat();
    }
    
    public JsonStringFormatter() {}
    
    @Override
    public void copyFrom(Object desjonizedObject) {
        JsonStringFormatter jsonStringFormatter = (JsonStringFormatter) desjonizedObject;
        this.stringFormat = jsonStringFormatter.getStringFormat();
    }
    
    public String getStringFormat() {
        return this.stringFormat;
    }
    
    public String toString() {
        return "stringFormatter";
    }

    @Override
    public IFormatter getIFormatter() {
        StringFormatter stringFormatter = new StringFormatter();
        stringFormatter.setStringFormat(this.stringFormat);
        return stringFormatter;
    }
}
