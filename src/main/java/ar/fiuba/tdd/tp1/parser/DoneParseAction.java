package ar.fiuba.tdd.tp1.parser;

// Represents the action of finishing parsing in an LR1 parser.
public class DoneParseAction implements LR1ParseAction {

    @Override
    public void apply(LR1Parser parser) {
        parser.done = true;
    }

}
