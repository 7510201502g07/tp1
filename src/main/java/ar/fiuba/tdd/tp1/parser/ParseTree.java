package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.lexer.Token;

import java.util.List;

// Output of a parser. It can be later processed using an interpreter. 
public interface ParseTree {

    public void setNode(Token node);

    public Token getNode();

    public void setParent(ParseTree parent);

    public ParseTree getParent();

    public void addChild(ParseTree child);

    public List<ParseTree> getChildren();
}
