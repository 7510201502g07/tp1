package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.contextfreegrammar.LR1ProductionRule;

// Represents the action of reducing by a certain rule in an LR1 parser.
public class ReduceParseAction implements LR1ParseAction {

    LR1ProductionRule rule = null;

    public ReduceParseAction(LR1ProductionRule rule) {
        this.rule = rule;
    }

    @Override
    public void apply(LR1Parser parser) throws Exception {

        this.rule.apply(parser.stateStack, parser.parseStack);
        String nextState = parser.stateTable.getNextState(parser.stateStack.peek(), this.rule.getResult());
        parser.stateStack.push(nextState);
    }

}
