package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.contextfreegrammar.ExtendedContextFreeGrammar;
import ar.fiuba.tdd.tp1.contextfreegrammar.LR1ParseTable;
import ar.fiuba.tdd.tp1.contextfreegrammar.LR1ParseTableBuilder;
import ar.fiuba.tdd.tp1.lexer.ConcreteLexer;
import ar.fiuba.tdd.tp1.lexer.Lexer;
import ar.fiuba.tdd.tp1.lexer.Token;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

public class LR1Parser implements Parser {

    // TODO: initialize.
    Lexer            lexer;
    Deque<String>    stateStack;
    Deque<ParseTree> parseStack;
    Queue<Token>     parseTokens;
    LR1ParseTable    stateTable;
    boolean          done;

    public LR1Parser(ExtendedContextFreeGrammar grammar) throws Exception {

        LR1ParseTableBuilder builder = new LR1ParseTableBuilder();
        this.stateTable = builder.generateLR1FSM(grammar);
        this.lexer = new ConcreteLexer(grammar);

        this.stateStack = new LinkedList<String>();
        this.parseTokens = new LinkedList<Token>();
        this.parseStack = new LinkedList<ParseTree>();
        this.done = false;
    }

    @Override
    public ParseTree parse(String input) throws Exception {

        this.clear();
        parseTokens = this.lexer.lex(input);

        while (!this.done) {

            LR1ParseAction nextAction = this.stateTable.getNextAction(this.currentState(), this.lookahead().getType());
            nextAction.apply(this);
        }

        return this.parseStack.getFirst();
    }

    public void registerStateMachine(LR1ParseTable stateMachine) throws Exception {
        if (this.stateTable != null) {
            throw new Exception("This parser already has a state machine registered.");
        }
        this.stateTable = stateMachine;
    }

    private void clear() {
        this.done = false;
        this.parseStack.clear();
        this.stateStack.clear();
        this.stateStack.push(this.stateTable.getStartState());
    }

    Token lookahead() {
        return this.parseTokens.peek();
    }

    String currentState() {
        return this.stateStack.peek();
    }

}
