package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.lexer.Token;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class ConcreteParseTree implements ParseTree {

    Token            node     = null;
    ParseTree        parent   = null;
    Deque<ParseTree> children = null;

    @Override
    public void setNode(Token node) {
        this.node = node;
        this.children = new LinkedList<ParseTree>();
    }

    @Override
    public Token getNode() {
        return this.node;
    }

    @Override
    public void setParent(ParseTree parent) {
        this.parent = parent;
    }

    @Override
    public ParseTree getParent() {
        return this.parent;
    }

    @Override
    public void addChild(ParseTree child) {
        this.children.push(child);
    }

    @Override
    public List<ParseTree> getChildren() {
        List<ParseTree> childrenList = new ArrayList<ParseTree>(this.children);
        return childrenList;
    }
}
