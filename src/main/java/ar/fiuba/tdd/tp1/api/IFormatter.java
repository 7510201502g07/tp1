package ar.fiuba.tdd.tp1.api;

public interface IFormatter {

    public String applyFormat(String value);
    
    public String getType();
    
    public void setNewFormat(String formatter, String format);
}
