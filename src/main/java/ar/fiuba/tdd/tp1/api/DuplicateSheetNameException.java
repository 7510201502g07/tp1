package ar.fiuba.tdd.tp1.api;

// Exception thrown when trying to add a sheet to a book that already has
// a sheet with the same name in it.
public class DuplicateSheetNameException extends Exception {

    private static final long serialVersionUID = -2194226259791915257L;

}
