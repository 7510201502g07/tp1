package ar.fiuba.tdd.tp1.api;

// Exception thrown when trying to add a book with a name 
// that already exists.
public class DuplicateBookNameException extends Exception {

    private static final long serialVersionUID = 1L;

}
