package ar.fiuba.tdd.tp1.api;

import ar.fiuba.tdd.tp1.undoredo.UndoRedo;
import ar.fiuba.tdd.tp1.workbook.CellDereferencer;
import ar.fiuba.tdd.tp1.workbook.ConcreteWorkbookBuilder;
import ar.fiuba.tdd.tp1.workbook.ConcreteWorkbookController;
import ar.fiuba.tdd.tp1.workbook.IWorkbookBuilder;

// This class is in charge of building a ConcreteWorkbookController. 
// Only one such controller should exist at a time. In other words,
// only call the method buildWorkbookController when you don't plan 
// on using the previous one anymore.
public class ConcreteWorkbookControllerBuilder implements IWorkbookControllerBuilder {

    @Override
    public IWorkbookController buildWorkbookController() {

        UndoRedo changesTracker = new UndoRedo();
        IWorkbookBuilder workbookBuilder = new ConcreteWorkbookBuilder(changesTracker);
        ConcreteWorkbookController newWorkbookController = new ConcreteWorkbookController(workbookBuilder,
                changesTracker);
        CellDereferencer.getInstance().setBookController(newWorkbookController);

        return newWorkbookController;
    }

}
