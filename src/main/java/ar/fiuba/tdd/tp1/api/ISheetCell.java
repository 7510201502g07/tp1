package ar.fiuba.tdd.tp1.api;

public interface ISheetCell {

    public String getId();

    // TODO: this exception.
    public void setValue(String value) throws Exception;

    public String getResult() throws InvalidFormulaException, InvalidSheetNameException, InvalidBookNameException;

    public String getRawValue();
    
    public void setType(String type);
    
    public String getType();
    
    public void setFormatter(IFormatter formatter);
    
    public IFormatter getFormatter();
    
    public String getFormattedResult() throws InvalidFormulaException, InvalidSheetNameException, InvalidBookNameException;

}