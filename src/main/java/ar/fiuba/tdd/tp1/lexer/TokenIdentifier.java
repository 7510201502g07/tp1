package ar.fiuba.tdd.tp1.lexer;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TokenIdentifier {

    String  type         = " ";
    Pattern regexPattern = null;
    Matcher matcher      = null;

    public TokenIdentifier(String type, String regex) {
        this.type = type;
        this.regexPattern = Pattern.compile(regex);
    }

    public boolean lookForToken(String input, int currentPosition) {
        this.matcher = this.regexPattern.matcher(input);
        return (this.matcher.find(currentPosition) && this.matcher.start() == currentPosition);
    }

    public int tokenLength() throws Exception {
        try {
            return this.matcher.group().length();
        } catch (Exception e) {
            throw new Exception("No token has been matched.");
        }
    }

    public Token getToken() throws Exception {
        try {
            return new Token(this.type, this.matcher.group());
        } catch (Exception e) {
            throw new Exception("No token has been matched.");
        }
    }

}
