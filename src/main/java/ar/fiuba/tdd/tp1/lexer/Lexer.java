package ar.fiuba.tdd.tp1.lexer;

import java.util.Queue;

public interface Lexer {

    public abstract Queue<Token> lex(String input) throws Exception;
}
